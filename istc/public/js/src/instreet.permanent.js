/*
*	permanent.js v0.3.3
*	+支持adsense同步代码以及异步代码
*/
;(function(window){

	var host = window.location.hostname
		,adUrl = ''
		,parentElement = null
		,now = new Date().getTime()
		,rdnum = Math.random()
		,isIE = !!window.ActiveXObject;

	// var pushAPI = 'http://popup.instreet.cn:3000/widget/push'
	var	pushAPI = 'http://istc.yylady.cn/widget/push'
		,afvUrl = 'http://s2.yylady.cn/corner/server/gnm.php'
		,dfpUrl='http://s2.yylady.cn/corner/server/justdoit_dfp.php'
		,mediavUrl='http://s2.yylady.cn/corner/boilerplate/popup_mediav.html'
		,normalUrl = 'http://s2.yylady.cn/corner/google.html'
		,doubleUrl = 'http://s2.yylady.cn/corner/google_double.html'
		,baiduUrl = 'http://s2.yylady.cn/corner/server/baidu.php'
		,bannerUrl='http://s2.yylady.cn/corner/server/gbanner.php'
		,regionMapUrl="http://s3.yylady.cn/corner/server/justdoit_dfp.php?dfp_id=5882886&dfp_name=yitejia-ceshi"
		;

	var util = {	//常用方法集
		bind : function(elem,e,fn){
			if(elem.addEventListener){
				elem.addEventListener(e,fn,false);
			}else if(elem.attachEvent){
				elem.attachEvent("on"+e,fn);
			}else{
				elem["on"+e] = fn;
			}
		},
		importFile  :function(type,name){
			var link,script,
			head=document.getElementsByTagName( "head" )[0] || document.documentElement;
			switch(type){
			case "js":
				script=document.createElement('script');
				script.async="async";
				script.charset="utf-8";
				script.type="text/javascript";
				script.onload=script.onreadystatechange=function() {
					if(!script.readyState || script.readyState === "loaded" || script.readyState === "complete"){
						script.onload = script.onreadystatechange = null;
						if ( head && script.parentNode ) {
						head.removeChild( script );
						}
					}
				};
				script.src=name;
				head.appendChild(script);
				break;
			case "css":
				link = document.createElement("link");link.type = "text/css";link.rel = "stylesheet";
				link.href=name;
				head.appendChild(link);
				break;
			}
		},
		createFrame : function(option){
			var frame = document.createElement('iframe'),
				options = {
					src : option.src,
					scrolling : 'no',
					height : option.height,
					width : option.width,
					frameBorder : "0",
					marginWidth : "0",
					marginHeight : "0"

				};
			for(var p in options){
				frame[p] = options[p];
			}
			return frame;
		}
	};

	var render=function(config){	// 展示广告
		if(config){
			var adType=config.adType||'adsense'
				,async = config.async||false
				,pubID=config.pubID
				,slot=config.slot
				,noframe=config.noframe
				,doubleFrame=config.doubleFrame||0
				,w=config.width
				,h=config.height
				,frameStr=''
				,adsenseUrl = config.doubleFrame?doubleUrl : normalUrl
				,adCode = ''
				,container
				,now = new Date().valueOf();
			// set adUrl and adCode
			switch(adType.toLowerCase()){
				case 'banner':
					var ad1Para=config.pubID1?'cad1='+config.pubID1+'&slot1='+config.slot1+'&w1='+config.w1+'&h1='+config.h1:'';
					var ad2Para=config.pubID2?'&cad2='+config.pubID2+'&slot2='+config.slot2+'&w2='+config.w2+'&h2='+config.h2:'';
					adUrl=bannerUrl+'?'+ad1Para+ad2Para+'&w='+w+'&h='+h+'&backColor='+(config.backColor||'f1f1f1')+'&frame='+doubleFrame+'&from='+host;
					break;
				case 'afv':
					adUrl+=afvUrl+'?pubID='+pubID+'&slot='+config.slot+'&width='+w+'&height='+h;
					break;
				case 'dfp':
					adUrl = dfpUrl+'?dfp_id='+config.dfpID+'&dfp_name='+config.dfpName+'&w='+(w>970?970:w)+'&h='+h;					
					// permanent.min.js会包含gpt.js
					adCode += "<script type='text/javascript'>googletag.defineSlot('/"
							+ config.dfpID
							+"/"
							+ config.dfpName
							+ "', ["
							+ w
							+ ", "
							+ h
							+ "], 'div-gpt-ad-"
							+ now
							+ "-0').addService(googletag.pubads()); googletag.pubads().enableSyncRendering(); googletag.enableServices(); </"
							+ "script><div id='div-gpt-ad-"
							+ now
							+ "-0'> <script type='text/javascript'> googletag.display('div-gpt-ad-"
							+ now
							+ "-0'); </"
							+ "script> </div>";
					break;
				case 'adm': //百度管家
					adCode = '<script type="text/javascript" >BAIDU_CLB_SLOT_ID = "'+pubID+'";</script> <script type="text/javascript" src="http://cbjs.baidu.com/js/o.js"></script>';
					break;					
				case 'baidu':
					adUrl = baiduUrl + '?pubID='+pubID;
					window.cpro_id = pubID;
					adCode = '<script src="http://cpro.baidustatic.com/cpro/ui/c.js" type="text/javascript"></'
							+ 'script>';
					break;
				default: // default adsense ad	
					if(async){			
						adUrl=adsenseUrl+'?cad='+pubID+'&slot='+slot+'&w='+w+'&h='+h+'&from='+host;
						var gadjs = 'http://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js';
						util.importFile('js',gadjs);
						adCode = '<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></'
								+ 'script><ins class="adsbygoogle" style="display:block;"  data-ad-format="horizontal,rectangle" data-ad-client="'
								+ pubID
								+'" data-ad-slot="'
								+ slot
								+ '"></ins><script>(adsbygoogle = window.adsbygoogle || []).push({});</'
								+ 'script>';
					}else{
						adCode = '<script type="text/javascript"> google_ad_client = "'
							+pubID
							+'"; google_ad_slot = "'
							+slot
							+'"; google_ad_width = '
							+w
							+', google_ad_height = '
							+h
							+'; </script> <script type="text/javascript" src="http://pagead2.googlesyndication.com/pagead/show_ads.js"></script>';
					}									
			}
			// 广告素材为js代码
			if(adCode){
				var cid = 'ins-permanent-container-'+(new Date().valueOf());
				document.write('<div id="'+cid+'">'+adCode+'</div>');
				container = document.getElementById(cid);
				// 使用adsense自适应代码
				if(adType=='adsense'&&async){
					(adsbygoogle = window.adsbygoogle || []).push({});
				}
			}else{
			// 广告素材为iframe
				container = document.createElement("div");
				var frame = util.createFrame({
					src : adUrl,
					width : w,
					height : h
				});
				container.appendChild(frame);
				parentElement.appendChild(container);
			}		

			container.style.cssText = 'position:relative;';			
			addTracker(config.trackUrl,container);	// add cnzz
		}
	};

	var addTracker = function  (url,container) {
		if(url){
			var cnzz = document.createElement("script")
				,span = document.createElement('span')
				;
			cnzz.type = "text/javascript";
			cnzz.src = url;
			var id = getUrlPara(url.substr(url.indexOf('?')+1)).id;
			span.id = 'cnzz_stat_icon_'+id;
			span.style.display = 'none';
			container.appendChild(span);
			container.appendChild(cnzz);
		}
		function getUrlPara(suffixStr){
			var r = /([^&=\?]+)=?([^&]*)/g
				;
			var a,b={};
			while((a=r.exec(suffixStr))){
				b[a[1]]=a[2];
			}
			return b;
		}	
	};

	var init=function(){  //初始化config
		if(typeof instreet_widgetSid!='undefined'){
			var field=instreet_widgetSid;
			parentElement = document.getElementById(field).parentNode ;
			if(parentElement.tagName == 'HEAD'){
				parentElement = document.documentElement;
			}
			instreet_widgetSid=null;
			if(typeof window.Instreet_StartPermanent == 'undefined'){
				window.Instreet_StartPermanent = render;
			}
			var ref = location.href;
			// 去掉引号
			ref = ref.replace(/'|''/g,'');
			var src = pushAPI+'?field='+field+'&url='+encodeURIComponent(ref)+'&callback=Instreet_StartPermanent&t='+new Date().getTime();
			document.write("<script type='text/javascript' src='"+src+"'></"+"script>");
		}
	};

	init();

})(window);
